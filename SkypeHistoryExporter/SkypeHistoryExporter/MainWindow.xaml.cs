﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SkypeHistoryExporter
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        SQLiteConnection cn = new SQLiteConnection();
        List<string> FilesResList = new List<string>();
        List<string> FilesResOutList = new List<string>();
        List<Contact> ContactsList = new List<Contact>();
        List<Message> MessagesList = new List<Message>();
        public MainWindow()
        {
            InitializeComponent();
            //Func.checkSkypeRun();
            goBackImageButton.Focusable = false;
            menuButton.Focusable = false;
        }

        private void search1Button_Click(object sender, RoutedEventArgs e)
        {
            FilesResList.Clear();
            FilesResOutList.Clear();
            var skypeDataDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Skype";
            //skypeDataDirectory = skypeDataDirectory;
            DirectoryInfo dir = new DirectoryInfo(skypeDataDirectory);
            string file = "main.db";
            Func.FindInDir(dir, file, true, ref FilesResList, ref FilesResOutList); //true = bool recursive
            
            //далее проверяем результат и даем выбрать логин
            checkSearchResult();


        }

        private void checkSearchResult()
        {
            if (FilesResList.Count <= 0)
            {
                MessageBox.Show("К сожалению файл с историей не найден! По возможности попробуйте другие методы поиска.",
                        "Пусто",
                         MessageBoxButton.OK,
                            MessageBoxImage.Information);
            }
            else
            {
                //если нашел
                Page1.Visibility = Visibility.Collapsed;
                Page2.Visibility = Visibility.Visible;
                resultsLoginsListBox.Items.Clear();
                resultsLoginsListBox.ItemsSource = null;
                resultsLoginsListBox.ItemsSource = FilesResOutList;
            }
        }

        

        

        private void goBackImageButton_Click(object sender, RoutedEventArgs e)
        {
            Page1.Visibility = Visibility.Visible;
            Page2.Visibility = Visibility.Collapsed;
            resultsLoginsListBox.SelectedItem = null;
            resultsLoginsListBox.ItemsSource = null;
            resultsLoginsListBox.Items.Clear();          
            //+ незабыть добавить очистку остального)
            contactsDataGrid.SelectedItem = null;
            contactsDataGrid.ItemsSource = null;
            contactsDataGrid.Items.Clear();
            Db.close(ref cn);
            filterTextBox.Text = null;
            findResLabel.Content = null;
            exportButton.IsEnabled = false;
            FilesResList.Clear();
            FilesResOutList.Clear();
            ContactsList.Clear();
            MessagesList.Clear();
        }

        private void resultsLoginsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (resultsLoginsListBox.SelectedItem == null) return;
            //вот тут меняем
            string mainDbFile = FilesResList[resultsLoginsListBox.SelectedIndex];
            Db.close(ref cn);
            Db.open(ref cn, mainDbFile);
            Db.getContactsFromDb(ref cn, ref ContactsList);
            contactsDataGrid.ItemsSource = null;
            contactsDataGrid.ItemsSource = ContactsList;
            //ListViewChats.ItemsSource = ContactsList;
            //Db.getMessagesFromDb(ref cn, ref MessagesList);
            //MessagesList = MessagesList;

        }

        private void contactsDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (contactsDataGrid.SelectedItem == null) return;
            Contact contObj = (Contact)contactsDataGrid.SelectedItem;
            Db.getMessagesFromDb(ref cn, ref MessagesList, contObj.skypename);
            //int mesCount = Db.getCountMessagesFromDb(ref cn, contObj.skypename);
            findResLabel.Content = "Найдено сообщений: " + MessagesList.Count;
            //MessagesList = MessagesList;
            if (MessagesList.Count > 0)
                exportButton.IsEnabled = true;
            else exportButton.IsEnabled = false;
        }

        private void filterTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            //фильтр списка по имени или логину
            contactsDataGrid.ItemsSource = null;
            List<Contact> ContactsListTmp = new List<Contact>();

            foreach (var cont in ContactsList)
            {
                if (cont.skypename.ToUpper().Contains(filterTextBox.Text.ToUpper()) || cont.fullname.ToUpper().Contains(filterTextBox.Text.ToUpper()) || cont.given_displayname.ToUpper().Contains(filterTextBox.Text.ToUpper()))
                    ContactsListTmp.Add(cont);
            }

            contactsDataGrid.ItemsSource = ContactsListTmp;

        }

        private void exportButton_Click(object sender, RoutedEventArgs e)
        {
           
            SaveDialog SaveDialogObj = Func.historySaveDialog();

            ProgressBar1.Visibility = Visibility.Visible;

            Task<Boolean>.Factory.StartNew(() =>
            {
                if (SaveDialogObj.formatIndex == 1)
                    Func.historyExportToHtm(MessagesList, SaveDialogObj.fileName, SaveDialogObj.dateTimeNowSave);
                if (SaveDialogObj.formatIndex == 2)
                    Func.historyExportToTxt(MessagesList, SaveDialogObj.fileName, SaveDialogObj.dateTimeNowSave);
                Thread.Sleep(2 * 1000);
                return true;
            }).ContinueWith(t =>
            {
                if (t.Result)
                {
                    //метод отработал
                    ProgressBar1.Visibility = Visibility.Collapsed;
                    MessageBox.Show("Готово! История с данным контактом сохранена.",
                       "Успешно",
                        MessageBoxButton.OK,
                           MessageBoxImage.Information);
                }
            }, TaskScheduler.FromCurrentSynchronizationContext());  

                
            

            //ProgressBar1.Visibility = Visibility.Collapsed;
            //MessageBox.Show("Готово!");
        }

        private void search2Button_Click(object sender, RoutedEventArgs e)
        {
            ProgressBar1.Visibility = Visibility.Visible;

            FilesResList.Clear();
            FilesResOutList.Clear();
            //
            DriveInfo[] allDrives = DriveInfo.GetDrives();

            Task<Boolean>.Factory.StartNew(() =>
            {
                foreach (DriveInfo d in allDrives)
                {
                    Console.WriteLine("Drive {0}", d.Name);
                    //[любой диск]:\Users\[любая папка]\AppData\Roaming\Skype
                    //но лучше с учетом windows.old
                    //еще и так
                    //[любой диск]:\[любая папка]\Users\[любая папка]\AppData\Roaming\Skype

                    advancedSearchOnePartMethod(d);
                    advancedSearchTwoPartMethod(d);
                }
                Thread.Sleep(3 * 1000);
                return true;
            }).ContinueWith(t =>
            {
                if (t.Result)
                {
                    //метод отработал
                    ProgressBar1.Visibility = Visibility.Collapsed;
                    //далее проверяем результат и даем выбрать логин
                    checkSearchResult();
                }
            }, TaskScheduler.FromCurrentSynchronizationContext());  

           
        }


        private void advancedSearchOnePartMethod(DriveInfo d)
        {
            //[любой диск]:\Users\[любая папка]\AppData\Roaming\Skype
            string testDirectory = d.Name + "Users";
            if (Directory.Exists(testDirectory))
            {
                searchInUsersFolderMethod(testDirectory);
            }
        }





        private void advancedSearchTwoPartMethod(DriveInfo d)
        {
            //[любой диск]:\[любая папка]\Users\[любая папка]\AppData\Roaming\Skype
            DirectoryInfo disk = new DirectoryInfo(d.Name);
            try
            {
                foreach (DirectoryInfo subdir in disk.GetDirectories())
                {
                    string testDirectory2 = subdir.FullName + @"\Users";
                    Console.WriteLine(testDirectory2);
                    if (Directory.Exists(testDirectory2))
                    {
                        searchInUsersFolderMethod(testDirectory2);
                    }
                }
            }
            catch (Exception)
            {

            }



        }


        private void searchInUsersFolderMethod(string testDirectory)
        {
            Console.WriteLine("Существует: " + testDirectory);
            DirectoryInfo dir = new DirectoryInfo(testDirectory);
            foreach (DirectoryInfo subdir in dir.GetDirectories())
            {
                string testDirectory2 = subdir.FullName + @"\AppData\Roaming\Skype";
                //testDirectory2 = testDirectory2;
                if (Directory.Exists(testDirectory2))
                {
                    //если папка со скайпом суш, то ищем маин файл
                    DirectoryInfo dir2 = new DirectoryInfo(testDirectory2);
                    string file = "main.db";
                    Func.FindInDir(dir2, file, true, ref FilesResList, ref FilesResOutList); //true = bool recursive

                }
            }
        }

        private void selectFileHimselfButton_Click(object sender, RoutedEventArgs e)
        {
            string openFilePath = Func.mainOpenFileDialog();
            if (openFilePath == null) return;

            FileInfo file = new FileInfo(openFilePath);

            FilesResList.Add(file.FullName);

            string s = file.Directory.Name + @"\" + file.Name;
            var fileSize = file.Length / 1024;
            FilesResOutList.Add(s + " | " + fileSize + " kb");

            //далее проверяем результат и даем выбрать логин
            checkSearchResult();
        }

        private void aboutImgButton_MouseUp(object sender, MouseButtonEventArgs e)
        {
            //MessageBox.Show("Up Click!!!!");
            WPFAboutBox1 w4 = new WPFAboutBox1();
            w4.Owner = this; //решает проблемы с перекрытием окон
            w4.ShowDialog();
        }

        private void menuButton_Click(object sender, RoutedEventArgs e)
        {
            menuButton.ContextMenu.IsOpen = !menuButton.ContextMenu.IsOpen;
        }

        private void exportAll(int formatIndex, string folder)
        {

            ProgressBar1.Visibility = Visibility.Visible;
            ProgressBar1.IsIndeterminate = false;
            resultsLoginsListBox.IsEnabled = false;
            menuButton.IsEnabled = false;

            Task<Boolean>.Factory.StartNew(() =>
            {
                var dateTimeNowSave = DateTime.Now;
                for (int i = 0; i < ContactsList.Count; i++)
                {
                    var contact = ContactsList[i];
                    Db.getMessagesFromDb(ref cn, ref MessagesList, contact.skypename);
                    if (MessagesList.Count > 0)
                    {
                        string FileName = Func.skypenameFix(contact.skypename) + "_" + dateTimeNowSave.ToString("dd_MM_yyyy-hh_mm_ss");
                        var dir = Directory.CreateDirectory(folder + System.IO.Path.DirectorySeparatorChar + "skype_history_backup_" + dateTimeNowSave.ToString("dd_MM_yyyy-hh_mm_ss"));
                        if (formatIndex == 1)
                        {
                            string fpath = dir.FullName + System.IO.Path.DirectorySeparatorChar + FileName + ".htm";
                            Func.historyExportToHtm(MessagesList, fpath, dateTimeNowSave);
                        }
                        if (formatIndex == 2)
                        {
                            string fpath = dir.FullName + System.IO.Path.DirectorySeparatorChar + FileName + ".txt";
                            Func.historyExportToTxt(MessagesList, fpath, dateTimeNowSave);
                        }
                    }
                    var progress = 100 * i / ContactsList.Count;
                    if (progress > 100.0) progress = 100;
                    //ProgressBar1.Value = progress;
                    Dispatcher.Invoke((Action)(() => ProgressBar1.Value = progress));
                }
                return true;
            }).ContinueWith(t =>
            {
                if (t.Result)
                {
                    //метод отработал
                    ProgressBar1.Visibility = Visibility.Collapsed;
                    ProgressBar1.IsIndeterminate = true;
                    resultsLoginsListBox.IsEnabled = true;
                    menuButton.IsEnabled = true;
                    MessageBox.Show("Готово! Вся история была сохранена.",
                       "Успешно",
                        MessageBoxButton.OK,
                           MessageBoxImage.Information);
                }
            }, TaskScheduler.FromCurrentSynchronizationContext());

        }

        private void exportAllHtmBut_Click(object sender, RoutedEventArgs e)
        {
            if (ContactsList.Count == 0) return;
            string openFolderPath = Func.mainOpenFolderDialog();
            if (openFolderPath == null) return;
            exportAll(1, openFolderPath);
        }

        private void exportAllTxtBut_Click(object sender, RoutedEventArgs e)
        {
            if (ContactsList.Count == 0) return;
            string openFolderPath = Func.mainOpenFolderDialog();
            if (openFolderPath == null) return;
            exportAll(2, openFolderPath);
        }

        private void hideContactsWith0MesBut_Click(object sender, RoutedEventArgs e)
        {
            if (ContactsList.Count == 0) return;
            //if (ContactsList.Any(x => x.messages_count > 0)) return;
            ProgressBar1.Visibility = Visibility.Visible;
            if (!ContactsList.Any(x => x.messages_count > 0))
                ProgressBar1.IsIndeterminate = false;
            resultsLoginsListBox.IsEnabled = false;
            menuButton.IsEnabled = false;

            Task<Boolean>.Factory.StartNew(() =>
            {
                if (!ContactsList.Any(x => x.messages_count > 0))
                for (int i = 0; i < ContactsList.Count; i++)
                {
                    int mesCount = Db.getCountMessagesFromDb(ref cn, ContactsList[i].skypename);
                    ContactsList[i].messages_count = mesCount;
                    //
                    var progress = 100 * i / ContactsList.Count;
                    if (progress > 100.0) progress = 100;
                    //ProgressBar1.Value = progress;
                    Dispatcher.Invoke((Action)(() => ProgressBar1.Value = progress));
                }
                ContactsList.RemoveAll(x => x.messages_count == 0);
                return true;
            }).ContinueWith(t =>
            {
                if (t.Result)
                {
                    //метод отработал
                    contactsDataGrid.ItemsSource = null;
                    contactsDataGrid.ItemsSource = ContactsList;
                    ProgressBar1.Visibility = Visibility.Collapsed;
                    ProgressBar1.IsIndeterminate = true;
                    resultsLoginsListBox.IsEnabled = true;
                    menuButton.IsEnabled = true;
                    MessageBox.Show("Готово! Контакты с 0 сообщениями были скрыты из списка.",
                       "Успешно",
                        MessageBoxButton.OK,
                           MessageBoxImage.Information);
                }
            }, TaskScheduler.FromCurrentSynchronizationContext());
            
        }

        private void contactsDataGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            PropertyDescriptor propertyDescriptor = (PropertyDescriptor)e.PropertyDescriptor;
            e.Column.Header = propertyDescriptor.DisplayName;
            if (propertyDescriptor.DisplayName == "messages_count" && !ContactsList.Any(x => x.messages_count > 0))
            {
                e.Cancel = true;
            }
        }


    }
}
