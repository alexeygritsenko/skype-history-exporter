﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SkypeHistoryExporter
{
    public class Contact
    {
        public string skypename { get; set; }
        public string fullname { get; set; }
        public string given_displayname { get; set; }
        public int messages_count { get; set; }
    }

    public class Message
    {
        public string from_dispname { get; set; } //автор данного сообщния
        public string time { get; set; }
        public string body_xml { get; set; }
        //
        //
        public string displayname { get; set; } //участники переписки //не експортиовать
        public string conversationsidentity { get; set; } //логин оппонента //не експортиовать
        public string messageschatname { get; set; } //не експортиовать
       
    }


    public class SaveDialog
    {
        public string fileName { get; set; }
        public DateTime dateTimeNowSave { get; set; }
        public int formatIndex { get; set; }
    }



}
