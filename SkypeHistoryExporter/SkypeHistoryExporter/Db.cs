﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Windows;

namespace SkypeHistoryExporter
{
    static class Db
    {

        static public void open(ref SQLiteConnection conn, string mainDbFilePath)
        {
            //SqlConnection cn = new SqlConnection();
            //cn.ConnectionString = "Data Source=(LocalDB)\v11.0;AttachDbFilename=|DataDirectory|\buro.mdf;Integrated Security=True"; //здесь ошибка
            //cn.ConnectionString = @"Data Source=(LocalDB)\v11.0;
            //         AttachDbFilename=|DataDirectory|\buro.mdf;";

            conn = new SQLiteConnection("Data Source=" + mainDbFilePath + "; Version=3;");
            try
            {
                conn.Open();
            }
            catch (SQLiteException ex)
            {
                Console.WriteLine(ex.Message);
                MessageBox.Show(ex.Message);
            }
        }

        static public void close(ref SQLiteConnection conn)
        {
            conn.Close();
        }


        static public void getContactsFromDb(ref SQLiteConnection conn, ref List<Contact> ContactsList)
        {
            ContactsList.Clear();
            
            SQLiteCommand cmd = conn.CreateCommand();
            string sql_command = @"select skypename, fullname, given_displayname from contacts";

            SQLiteCommand myCommand = new SQLiteCommand(sql_command, conn);

            using (SQLiteDataReader dr = myCommand.ExecuteReader()) { 
            while (dr.Read())
            {
                //Console.WriteLine(dr[0]);
                //MessageBox.Show(" " + dr[0]);
                Contact cont = new Contact() { skypename = dr[0].ToString(), fullname = dr[1].ToString(), given_displayname = dr[2].ToString() };
                ContactsList.Add(cont);
            }
        }
            //conn.Close();
        }


        static public void getMessagesFromDb(ref SQLiteConnection conn, ref List<Message> MessagesList, string partnerLogin)
        {
            MessagesList.Clear();

            SQLiteCommand cmd = conn.CreateCommand();
            string sql_command = @"select 
       conversations.displayname, 
	   conversations.identity,
       messages.from_dispname,  
	   messages.chatname,
       strftime('%d.%m.%Y %H:%M:%S',messages.timestamp, 'unixepoch', 'localtime'), 
       messages.body_xml
  from conversations
       inner join messages on conversations.id = messages.convo_id
   where messages.chatname = '"+partnerLogin+"' OR conversations.identity = '"+partnerLogin+"'" +
    "order by messages.timestamp";

            SQLiteCommand myCommand = new SQLiteCommand(sql_command, conn);

            using (SQLiteDataReader dr = myCommand.ExecuteReader())
            {
                while (dr.Read())
                {
                    //Console.WriteLine(dr[0]);
                    //MessageBox.Show(" " + dr[0]);
                    Message mess = new Message() { from_dispname = dr[2].ToString(), time = dr[4].ToString(), body_xml = dr[5].ToString() };
                    MessagesList.Add(mess);
                }
            }
            //conn.Close();
        }


        static public int getCountMessagesFromDb(ref SQLiteConnection conn, string partnerLogin)
        {
            string sql_command = @"select count(*) from conversations
       inner join messages on conversations.id = messages.convo_id
   where messages.chatname = '" + partnerLogin + "' OR conversations.identity = '" + partnerLogin + "'";

            SQLiteCommand cmd = new SQLiteCommand(sql_command, conn);
            //cmd.CommandType = System.Data.CommandType.Text;
            int rowCount = Convert.ToInt32(cmd.ExecuteScalar());
            //Console.WriteLine("rowCount= " + rowCount);
            return rowCount;
        }


    }
}
